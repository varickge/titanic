import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    x = 0
    y = 0
    k = 0
    m = 0
    l = 0
    n = 0
    c_mr = 0
    c_mrs = 0
    c_miss = 0

    for index, row in df.iterrows():
        pos = row.Name.split()[1]   

        if row.isna().Age:
            if pos == 'Mr.':
                x += 1
            elif pos == 'Mrs.':
                k += 1
            elif pos == 'Miss.':
                l += 1
            continue

        if pos == 'Mr.':
            y += row.Age
            c_mr += 1
        elif pos == 'Mrs.':
            m += row.Age
            c_mrs += 1
        elif pos == 'Miss.':
            n += row.Age
            c_miss += 1
        
    


    return [('Mr.', x, int(y // c_mr)), ('Mrs.', k, int(m // c_mrs)), ('Miss.', l, int(n // c_miss))]